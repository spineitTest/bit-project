CREATE OR REPLACE
PROCEDURE GM_IT_MOVE_REGION(
    p_region    IN T708_REGION_ASD_MAPPING.C901_REGION_ID%TYPE,
    p_old_zone  IN T710_SALES_HIERARCHY.C901_ZONE_ID%TYPE,
    p_new_zone  IN T710_SALES_HIERARCHY.C901_ZONE_ID%TYPE,
    p_old_VP_ID IN T708_REGION_ASD_MAPPING.C101_USER_ID%TYPE,
    P_NEW_VP_ID IN T708_REGION_ASD_MAPPING.C101_USER_ID%TYPE
   )
AS
  REGION_NAME T901_CODE_LOOKUP.C901_CODE_NM%TYPE;
  ZONE_NAME_OLD T901_CODE_LOOKUP.C901_CODE_NM%TYPE;
  ZONE_NAME_NEW T901_CODE_LOOKUP.C901_CODE_NM%TYPE;
  OLD_VP_NAME T101_USER.C101_USER_F_NAME%TYPE;
  NEW_VP_NAME T101_USER.C101_USER_F_NAME%TYPE;
  SALES_HIERARCHY_ID T710_SALES_HIERARCHY.C710_SALES_HIERARCHY_ID%TYPE;
  REGION_ASD_ID T708_REGION_ASD_MAPPING.C708_REGION_ASD_ID%TYPE;
  Division_id  T710_SALES_HIERARCHY.C901_DIVISION_ID%TYPE;
  Company_id  T710_SALES_HIERARCHY.C901_COMPANY_ID%TYPE;
    
  
  -- Getting the code names and the user names
BEGIN
  REGION_NAME  :=GET_CODE_NAME(p_region);
  ZONE_NAME_OLD:=GET_CODE_NAME(p_old_zone);
  ZONE_NAME_NEW:=GET_CODE_NAME(p_new_zone);
  old_VP_NAME  :=GET_USER_NAME(p_old_VP_ID);
  NEW_VP_NAME  :=GET_USER_NAME(P_NEW_VP_ID);
 
  -- Fetch the existing t710_sales_hierarchy_id which needs to be voided for region-zone change

BEGIN
  SELECT C710_SALES_HIERARCHY_ID, C901_DIVISION_ID,C901_COMPANY_ID
  INTO SALES_HIERARCHY_ID,Division_id,Company_id
  FROM T710_SALES_HIERARCHY
  WHERE C901_AREA_ID=P_REGION
  AND C710_VOID_FL IS NULL
  AND C710_ACTIVE_FL='Y'
  AND C901_ZONE_ID  =p_old_zone;
EXCEPTION
WHEN NO_DATA_FOUND THEN
 raise_application_error('-20981','No Region-Zone mapping found');
WHEN OTHERS THEN
 raise_application_error('-20981','More than 1 Region Zone mapping found');
  END;

-- Void the existing Region-Zone Mapping
BEGIN
  IF SALES_HIERARCHY_ID='' THEN
   raise_application_error('-20981','No Region Zone Mapping');
  ELSE
    UPDATE T710_SALES_HIERARCHY
    SET C710_ACTIVE_FL           = NULL,
      C710_VOID_FL               ='Y',
      C710_LAST_UPDATED_BY       =2277820,
      C710_LAST_UPDATED_DATE     =SYSDATE
    WHERE C710_SALES_HIERARCHY_ID=SALES_HIERARCHY_ID
    AND C901_AREA_ID             =p_region
    AND C901_zone_id             =p_old_zone;
    
-- DBMS_OUTPUT.PUT_LINE('Old Zone-Region Mapping removed '||REGION_NAME||'and Zone'||ZONE_NAME_OLD);
 End IF; 
  END;

    -- Insert the New Region-Zone Mapping
    INSERT
    INTO T710_SALES_HIERARCHY
      (
        C710_SALES_HIERARCHY_ID,
        C901_DIVISION_ID,
        C901_COMPANY_ID,
        C901_ZONE_ID,
        C901_AREA_ID,
        C710_ACTIVE_FL,
        C710_VOID_FL,
        C710_CREATED_BY,
        C710_CREATED_DATE
      )
      VALUES
      (
        S710_SALES_HIERARCHY.NEXTVAL,
        Division_id,
        Company_id,
        p_new_zone,
        p_region,
        'Y',
        NULL,
        2277820,
        SYSDATE
     );
--  DBMS_OUTPUT.PUT_LINE('New Zone-Region Mapping completed '||REGION_NAME||'and Zone'||ZONE_NAME_NEW);

 
  
  -- Fetch the existing t708_region_asd_id which needs to be voided for existing VP
  
  BEGIN
    SELECT C708_REGION_ASD_ID
    INTO REGION_ASD_ID
    FROM T708_REGION_ASD_MAPPING
    WHERE C901_REGION_ID   =p_region
    AND C901_USER_ROLE_TYPE=8001
    AND C708_DELETE_FL    IS NULL
    AND C708_INACTIVE_FL  IS NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
  raise_application_error('-20981','No Region-VP mapping found');
  WHEN OTHERS THEN
raise_application_error('-20981','More than 1 VP mapping found for this region');
  END;
  
-- VOid the existing VP Mapping

    UPDATE T708_REGION_ASD_MAPPING
    SET C708_DELETE_FL      ='Y',
      C708_INACTIVE_FL      ='Y',
      C708_LAST_UPDATED_BY  =2277820,
      C708_LAST_UPDATED_DATE=SYSDATE
    WHERE C708_REGION_ASD_ID=REGION_ASD_ID AND
    C101_USER_ID=p_old_VP_ID;
    
--    DBMS_OUTPUT.PUT_LINE('Existing VP mapping voided for the region'||REGION_NAME||'and Zone'||ZONE_NAME_OLD||' with the user'||old_VP_NAME);
 

    INSERT
    INTO T708_REGION_ASD_MAPPING
      (
        C708_REGION_ASD_ID,
        C101_USER_ID,
        C901_REGION_ID,
        C708_START_DT,
        C708_END_DATE,
        C708_DELETE_FL,
        C708_CREATED_BY,
        C708_CREATED_DATE,
        C708_LAST_UPDATED_DATE,
        C708_LAST_UPDATED_BY ,
        C901_USER_ROLE_TYPE,
        C901_REF_ID,
        C708_INACTIVE_FL
      )
      VALUES
      (
        S708_REGION_ASD_MAPPING.NEXTVAL,
        P_NEW_VP_ID,p_region,
        SYSDATE,
        NULL,
        NULL,
        2277820,
        SYSDATE,
        NULL,
        NULL,
        8001,
        NULL,
        NULL
      );
   
--  DBMS_OUTPUT.PUT_LINE('New VP Assiged for the Region||REGION_NAME'||'and Zone'||ZONE_NAME_OLD||' with the user'||NEW_VP_NAME);
 
 END GM_IT_MOVE_REGION;
 /