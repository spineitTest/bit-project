-- @"C:\database\Procedures\Sales\GM_SAVE_ACCOUNT_ATTRIBUTE.prc";
/**************************************************************************************************************
* Description    : This procedure is called to get Account Attribute Details for Account Setup Screen.
* Parameters    : p_accid, p_accInputStr, p_action, p_userid
*       :
**************************************************************************************************************/
CREATE OR REPLACE
PROCEDURE gm_save_account_attribute (
        p_accid IN t704_account.c704_account_id%TYPE,
        p_accInputStr IN VARCHAR2,
        p_action IN VARCHAR2,
        p_userid IN t704a_account_attribute.c704a_created_by%TYPE)
AS
    v_strlen    NUMBER := NVL (LENGTH (p_accInputStr), 0) ;
    v_string    VARCHAR2 (4000) := p_accInputStr;
    v_substring VARCHAR2 (1000) ;
    v_acc_attr_type t704a_account_attribute.c901_attribute_type%TYPE;
    v_acc_attr_val t704a_account_attribute.c704a_attribute_value%TYPE;
    v_account_attr_id t704a_account_attribute.c704a_account_attribute_id%TYPE;
BEGIN
    --
    IF v_strlen > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            --
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
            --
            v_acc_attr_type := NULL;
            v_acc_attr_val := NULL;
            --
            v_acc_attr_type := TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
            v_acc_attr_val := v_substring;
			--	        
            gm_pkg_sm_acct_trans.gm_sav_acct_attr(p_accid, v_acc_attr_val, v_acc_attr_type, p_userid);
            --
        END LOOP;
    END IF;
END gm_save_account_attribute;
/
