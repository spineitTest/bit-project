/* Formatted on 2011/08/04 16:00 (Formatter Plus v4.8.0) */
--@"c:\database\procedures\sales\gm_save_salesrep_table.prc";

CREATE OR REPLACE PROCEDURE gm_save_salesrep_table (
	p_firstnm		   IN		t101_party.c101_first_nm%TYPE
  , p_lastnm		   IN		t101_party.c101_last_nm%TYPE
  , p_repnmen          IN       t703_sales_rep.c703_sales_rep_name_en%TYPE
  , p_distid		   IN		t703_sales_rep.c701_distributor_id%TYPE
  , p_category		   IN		t703_sales_rep.c703_rep_category%TYPE
  , p_salesrepid	   IN		VARCHAR2
  , p_userid		   IN		t703_sales_rep.c703_created_by%TYPE
  , p_terrid		   IN		t703_sales_rep.c702_territory_id%TYPE
  , p_activefl		   IN		t703_sales_rep.c703_active_fl%TYPE
  , p_partyid		   IN		t703_sales_rep.c101_party_id%TYPE
  , p_startdt		   IN		t703_sales_rep.c703_start_date%TYPE
  , p_enddt 		   IN		t703_sales_rep.c703_end_date%TYPE
  , p_dsgn			   IN		t703_sales_rep.c901_designation%TYPE
  , p_source		   IN		VARCHAR2
  , p_primarydivid     IN		t1910_division.c1910_division_id%TYPE
  , p_out_salesrepid   OUT		VARCHAR2
)
AS
	v_email_id	   VARCHAR2 (200);
	v_company_id t1900_company.c1900_company_id%TYPE;
BEGIN
	
	SELECT get_compid_frm_cntx()
    		INTO v_company_id
   			FROM DUAL;
	
	v_email_id	:= NVL (get_user_emailid (p_salesrepid), 'djames@globusmedical.com');
	p_out_salesrepid := p_salesrepid;

	UPDATE t703_sales_rep
	   SET c703_sales_rep_name = p_firstnm || ' ' || p_lastnm
	   	 , c703_sales_rep_name_en = p_repnmen
		 , c701_distributor_id = p_distid
		 , c703_rep_category = p_category
		 , c702_territory_id = p_terrid
		 , c703_last_updated_date = CURRENT_DATE
		 , c703_last_updated_by = p_userid
		 , c703_active_fl = p_activefl
		 , c703_start_date = NVL(p_startdt,c703_start_date)
		 , c703_end_date = p_enddt
		 , c901_designation = p_dsgn
		 , c1910_division_id = NVL(p_primarydivid,c1910_division_id)
	 WHERE c703_sales_rep_id = NVL(p_salesrepid, -99999)
	   AND c1900_company_id = v_company_id;

	--p_rep_id := v_rep_id;
	IF (SQL%ROWCOUNT = 0)
	THEN
		--
		---Need to make sure sales rep id always the same as user ID.
		IF (NVL (p_source, '-') != 'USER_EDIT') 
		THEN
			SELECT s703_sales_rep.NEXTVAL
			  INTO p_out_salesrepid
			  FROM DUAL;
		END IF;

		--p_rep_id	:= v_rep_id;
		INSERT INTO t703_sales_rep
					(c703_sales_rep_id, c703_sales_rep_name, c703_sales_rep_name_en , c701_distributor_id, c703_rep_category, c702_territory_id
				   , c703_created_by, c703_created_date, c703_active_fl, c703_sales_fl, c101_party_id, c703_start_date
				   , c703_end_date, c901_designation, c703_email_id, c1900_company_id, c1910_division_id
					)
			 VALUES (p_out_salesrepid, p_firstnm || ' ' || p_lastnm, p_repnmen , p_distid, p_category, p_terrid
				   , p_userid, CURRENT_DATE, DECODE(NVL(p_activefl,'N'),'N','N','Y'), 'N', p_partyid, NVL(p_startdt, CURRENT_DATE)
				   , p_enddt, p_dsgn, v_email_id, v_company_id, p_primarydivid
					);
	END IF;
END gm_save_salesrep_table;
/
