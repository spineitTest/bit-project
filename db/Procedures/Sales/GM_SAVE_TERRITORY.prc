
--@"c:\database\procedures\sales\gm_save_territory.prc";
CREATE OR REPLACE PROCEDURE gm_save_territory (
	p_terrid	 IN 	  t702_territory.c702_territory_id%TYPE
  , p_terrnm	 IN 	  t702_territory.c702_territory_name%TYPE
  , p_year		 IN 	  NUMBER
  , p_inputstr	 IN 	  VARCHAR2
  , p_userid	 IN 	  t702_territory.c702_created_by%TYPE
  , p_activefl	 IN 	  t702_territory.c702_active_fl%TYPE
  , p_action	 IN 	  VARCHAR2
  , p_repid	 	 IN 	  t709_quota_breakup.c703_sales_rep_id%TYPE
 -- , p_currtype   IN 	  T709_QUOTA_BREAKUP.C901_CURN_TYPE%TYPE	
  , p_returnid	 OUT	  VARCHAR2
  , p_message	 OUT	  VARCHAR2
)
AS
	v_terr_id	   NUMBER;
	v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
	v_company_id   NUMBER ;
	
BEGIN
--
	IF p_action = 'Add'
	THEN
		--
		SELECT s702_territory.NEXTVAL
		  INTO v_terr_id
		  FROM DUAL;
		
		BEGIN
			SELECT C1900_COMPANY_ID
			  INTO v_company_ID
			  FROM T703_SALES_REP
			 WHERE C703_SALES_REP_ID = P_REPID;
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			v_company_id := '';
		END;
		--
		INSERT INTO t702_territory
					(c702_territory_id, c702_territory_name, c702_created_by, c702_created_date, c702_active_fl,C1900_COMPANY_ID
					)
			 VALUES (v_terr_id, p_terrnm, p_userid, SYSDATE, p_activefl, v_company_id
					);
	
		IF p_repid IS NOT NULL THEN
		 
			 	UPDATE t703_sales_rep 
			 	   SET c702_territory_id = v_terr_id
			 	     , c703_last_updated_by = p_userid
			         , c703_last_updated_date = SYSDATE 
			 	 WHERE c703_sales_rep_id = p_repid 
			 	   AND c703_void_fl IS NULL;

		END IF;	 
	--
	ELSE
		--
		v_terr_id	:= p_terrid;

		--
		UPDATE t702_territory
		   SET c702_territory_name = p_terrnm
			 , c702_active_fl = p_activefl
			 , c702_last_updated_by = p_userid
			 , c702_last_updated_date = SYSDATE
		 WHERE c702_territory_id = v_terr_id;
	--
	END IF;

	--
	IF v_strlen > 0 AND p_repid IS NOT NULL
	THEN
		gm_save_quota (p_repid, p_inputstr,  p_message);

	END IF;

	--
	p_returnid	:= v_terr_id;
	--
	dbms_mview.REFRESH ('v700_territory_mapping_detail');
--
END gm_save_territory;
/
