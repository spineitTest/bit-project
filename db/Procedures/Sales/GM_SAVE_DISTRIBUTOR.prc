/* Formatted on 2008/06/13 14:47 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PROCEDURE gm_save_distributor (
	p_distnm		IN		 t701_distributor.c701_distributor_name%TYPE
   ,p_distnmen		IN		 t701_distributor.c701_distributor_name_en%TYPE
  , p_region		IN		 t701_distributor.c701_region%TYPE
  , p_distnames 	IN		 t701_distributor.c701_distributor_incharge%TYPE
  , p_conperson 	IN		 t701_distributor.c701_contact_person%TYPE
  , p_billnm		IN		 t701_distributor.c701_bill_name%TYPE
  , p_shipnm		IN		 t701_distributor.c701_ship_name%TYPE
  , p_billadd1		IN		 t701_distributor.c701_bill_add1%TYPE
  , p_shipadd1		IN		 t701_distributor.c701_ship_add1%TYPE
  , p_billadd2		IN		 t701_distributor.c701_bill_add2%TYPE
  , p_shipadd2		IN		 t701_distributor.c701_ship_add2%TYPE
  , p_billcity		IN		 t701_distributor.c701_bill_city%TYPE
  , p_shipcity		IN		 t701_distributor.c701_ship_city%TYPE
  , p_billstate 	IN		 t701_distributor.c701_bill_state%TYPE
  , p_shipstate 	IN		 t701_distributor.c701_ship_state%TYPE
  , p_billcountry	IN		 t701_distributor.c701_bill_country%TYPE
  , p_shipcountry	IN		 t701_distributor.c701_ship_country%TYPE
  , p_billzip		IN		 t701_distributor.c701_bill_zip_code%TYPE
  , p_shipzip		IN		 t701_distributor.c701_ship_zip_code%TYPE
  , p_phone 		IN		 t701_distributor.c701_phone_number%TYPE
  , p_fax			IN		 t701_distributor.c701_fax%TYPE
  , p_startdate 	IN		 t701_distributor.c701_contract_start_date%TYPE 
  , p_enddate		IN		 t701_distributor.c701_contract_end_date%TYPE
  , p_comments		IN		 t701_distributor.c701_comments%TYPE
  , p_activefl		IN		 t701_distributor.c701_active_fl%TYPE
  , p_distid		IN		 t701_distributor.c701_distributor_id%TYPE
  , p_userid		IN		 t701_distributor.c701_created_by%TYPE
  , p_action		IN		 VARCHAR2
  , p_comnperc		IN		 t701_distributor.c701_commission_percent%TYPE
  , p_disttyp		IN		 t701_distributor.c901_distributor_type%TYPE
  , p_partypric 	IN		 t701_distributor.c101_party_intra_map_id%TYPE
  , p_short_nm      IN       t701_distributor.c701_dist_sh_name%TYPE
  , p_commtyp		IN		 t701_distributor.c901_commission_type%TYPE
  , p_message		OUT 	 VARCHAR2
  , p_dist_id		OUT 	 VARCHAR2
)
AS
--
	v_dist_id	   NUMBER;
	v_string	   VARCHAR2 (10);
	v_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
	v_company_id t1900_company.c1900_company_id%TYPE;
--
BEGIN
--

	SELECT get_compid_frm_cntx()
    		INTO v_company_id
   			FROM DUAL;
	
	IF p_action = 'Add'
	THEN
		--
		SELECT s701_distributor.NEXTVAL
		  INTO v_dist_id
		  FROM DUAL;

		--
		p_dist_id	:= v_dist_id;

		--
		INSERT INTO t701_distributor
					(c701_distributor_id, c701_distributor_name, c701_distributor_name_en, c701_region, c701_distributor_incharge
				   , c701_contact_person, c701_commission_percent, c701_bill_name, c701_ship_name, c701_bill_add1
				   , c701_ship_add1, c701_bill_add2, c701_ship_add2, c701_bill_city, c701_ship_city, c701_bill_state
				   , c701_ship_state, c701_bill_country, c701_ship_country, c701_bill_zip_code, c701_ship_zip_code
				   , c701_phone_number, c701_fax, c701_contract_start_date, c701_contract_end_date
				   , c701_comments, c701_active_fl, c701_created_by, c701_created_date, c901_distributor_type
				   , c101_party_intra_map_id
				   , c901_commission_type, c701_dist_sh_name, c1900_company_id
					)
			 VALUES (v_dist_id, p_distnm, p_distnmen, p_region, p_distnames
				   , p_conperson, p_comnperc, p_billnm, p_shipnm, p_billadd1
				   , p_shipadd1, p_billadd2, p_shipadd2, p_billcity, p_shipcity, p_billstate
				   , p_shipstate, p_billcountry, p_shipcountry, p_billzip, p_shipzip
				   , p_phone, p_fax, p_startdate, p_enddate
				   , p_comments, p_activefl, p_userid, CURRENT_DATE, p_disttyp
				   , p_partypric
				   , p_commtyp, p_short_nm, v_company_id
					);
	--
	ELSE
		--
		p_dist_id	:= p_distid;

		--
		UPDATE t701_distributor
		   SET c701_distributor_name = p_distnm
		     , c701_distributor_name_en = p_distnmen
			 , c701_region = p_region
			 , c701_distributor_incharge = p_distnames
			 , c701_contact_person = p_conperson
			 , c701_commission_percent = p_comnperc
			 , c701_bill_name = p_billnm
			 , c701_ship_name = p_shipnm
			 , c701_bill_add1 = p_billadd1
			 , c701_ship_add1 = p_shipadd1
			 , c701_bill_add2 = p_billadd2
			 , c701_ship_add2 = p_shipadd2
			 , c701_bill_city = p_billcity
			 , c701_ship_city = p_shipcity
			 , c701_bill_state = p_billstate
			 , c701_ship_state = p_shipstate
			 , c701_bill_country = p_billcountry
			 , c701_ship_country = p_shipcountry
			 , c701_bill_zip_code = p_billzip
			 , c701_ship_zip_code = p_shipzip
			 , c701_phone_number = p_phone
			 , c701_fax = p_fax
			 , c701_contract_start_date = p_startdate
			 , c701_contract_end_date = p_enddate
			 , c701_comments = p_comments
			 , c701_active_fl = p_activefl
			 , c701_last_updated_by = p_userid
			 , c701_last_updated_date = CURRENT_DATE
			 , c901_distributor_type = p_disttyp
			 , c101_party_intra_map_id = p_partypric
			 , c901_commission_type = p_commtyp
			 , c701_dist_sh_name = p_short_nm
		 WHERE c701_distributor_id = p_distid
           AND c1900_company_id = v_company_id;
	--
	END IF;
	
	-- create a new field sales warehouse
	    -- 3 (FS Warehouse)
		-- 4000338 (Field Sales - new location type)
    	-- 93310 (Active)
		gm_pkg_op_inv_field_sales_tran.gm_sav_fs_inv_location (3, p_dist_id, p_short_nm, 4000338, 93310, p_userid,
        	v_location_id) ;

	--
	dbms_mview.REFRESH ('v700_territory_mapping_detail');
--
END gm_save_distributor;
/
